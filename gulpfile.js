'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'), //следит за изменениями файлов
    sass = require('gulp-sass'), //компилятор sass
    sourcemaps = require('gulp-sourcemaps'), //map для sass
    rigger = require('gulp-rigger'),
    rimraf = require('gulp-rimraf'),
    browserSync = require("browser-sync"), // запуск сервера (как локального так и глобального)
    autoprefixer = require('gulp-autoprefixer'), // добавляем префиксы
    useref = require('gulp-useref'), // забираем нужные файлы из bower_components
    reload = browserSync.reload

// Прописываем пути
var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'build/',
        css: 'build/css/'
    },
    src: { //Пути откуда брать исходники
        html: 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        style: 'src/css/*.scss',
        css: 'src/css/*.css'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'src/*.html',
        template: 'src/template/*.html',
        style: 'src/css/main.scss',
    },
    clean: 'build'
};


// настройки dev сервера
var config = {
    server: {
        baseDir: "build"
    },
    tunnel: false,
    host: 'localhost',
    port: 9000,
    logPrefix: "FunBek"
};
// liverelood
gulp.task('webserver', function(){
    browserSync(config);
})

// Очистка из папки build
gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

// сборка html
gulp.task('html:build', function () {
    gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(useref({ searchPath: '.tmp' })) // Забираем файлы из bower_components
        .pipe(rigger()) //Прогоним через rigger
        //.pipe(htmlmin({collapseWhitespace: true})) // Сжимаем html
        .pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
        .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

// собираем css
gulp.task('style:build', function () {
    gulp.src(path.src.style) //Выберем наш main.scss
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError)) //Выдаем ошибки
        .pipe(autoprefixer( {
            browsers: ["last 20 version", "> 1%", "ie 8", "ie 7"], cascade: false
        }))
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(sourcemaps.write('.'))
        // .pipe(gzip())
        .pipe(gulp.dest(path.build.css)) //И в build
        .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

// Команда gulp build запустит все таски на сборку
gulp.task('build', [
    'html:build',
    'style:build'
]);

// следим за изменениями файлов
gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });

    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });

});



// Задаем начальную команду gulp на выполнение тасков и слежение элементов
gulp.task('default', ['build', 'watch', 'webserver'])